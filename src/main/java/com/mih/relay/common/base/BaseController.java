package com.mih.relay.common.base;

import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * © 2020 MIH, Inc. All rights reserved.
 *
 * Controller 공통 모듈
 *
 * BaseController.java 
 * version : 1.0.0 
 * creation : 2020. 12. 08. 
 * author : soft33
 */
public abstract class BaseController {

    protected Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    protected MessageSource messageSource;

    protected Locale mLocale = LocaleContextHolder.getLocale();

    protected String baseUrl;
    protected String viewRootPath;

    /**
     * viewRootPath 생성
     */
    public BaseController() {
        RequestMapping rm = (RequestMapping) getClass().getAnnotation(RequestMapping.class);
        if (rm != null) {
            this.baseUrl = rm.value()[0];
            this.viewRootPath = this.baseUrl.replaceFirst("^/", "") + "/";
        }
    }
}
