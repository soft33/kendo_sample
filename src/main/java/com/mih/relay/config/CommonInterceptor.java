package com.mih.relay.config;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

@Component
public class CommonInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
            throws Exception {
                HttpServletResponse httpServletResponse = (HttpServletResponse) response; 
                addSameSite(httpServletResponse , "None");
                return false;
    }
 
    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) throws Exception {
        
    }
 
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex)
            throws Exception {
        
    }

    private void addSameSite(HttpServletResponse response, String sameSite) { 
        Collection<String> headers = response.getHeaders(HttpHeaders.SET_COOKIE); 
        boolean firstHeader = true; 
        for (String header : headers) { // there can be multiple Set-Cookie attributes 
            if (firstHeader) { 
                response.setHeader(HttpHeaders.SET_COOKIE, String.format("%s; Secure; %s", header, "SameSite=" + sameSite)); 
                firstHeader = false; 
                continue; 
            } 
            response.addHeader(HttpHeaders.SET_COOKIE, String.format("%s; Secure; %s", header, "SameSite=" + sameSite)); 
        }
    }

}
