package com.mih.relay.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

 /**
 * Mvc관련 설정 application.properties 이용 설정
 *
 * WebMvcConfig.java
 *
 * version : 1.0.0
 * creation : 2020. 9. 17.
 * author : lcg
 */
@Configuration
@PropertySource("classpath:application.properties")
public class WebMvcConfig implements WebMvcConfigurer {
    /**
     * Registration Interceptors
     *
     * @param registry InterceptorRegistry
     */
    public void addInterceptors(InterceptorRegistry registry) {
    }
}
