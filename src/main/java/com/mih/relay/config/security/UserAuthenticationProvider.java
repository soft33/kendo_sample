package com.mih.relay.config.security;

import java.util.ArrayList;
import java.util.List;

import com.mih.relay.config.vo.User;
import com.mih.relay.site.member.domain.MemberEntity;
import com.mih.relay.site.member.repository.MemberRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class UserAuthenticationProvider implements AuthenticationProvider {
	
	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	MemberRepository memberRepository;

	@Override
	public Authentication authenticate(Authentication authentication) throws AuthenticationException {

		String name = authentication.getPrincipal().toString();
        String password = authentication.getCredentials().toString();
		// String encodePassword = passwordEncoder.encode(password);

		UsernamePasswordAuthenticationToken result = null;
		MemberEntity me = memberRepository.findByMemId(name);


		if(me != null && me.getMemPass().equalsIgnoreCase(password)){
			List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>();
			if("USER".equals(me.getMemRole())){
				authList.add(new SimpleGrantedAuthority("ROLE_USER"));
			}
			if("ADMIN".equals(me.getMemRole())){
				authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
			}

			result = new UsernamePasswordAuthenticationToken(
				name, password, authList);
			User user = new User();
			result.setDetails(user);
		}else{
			throw new BadCredentialsException("USER NOT FOUND");
		}

		return result;
	}

	@Override
	public boolean supports(Class<?> authentication) {
		return authentication.equals(UsernamePasswordAuthenticationToken.class);
	}
}
