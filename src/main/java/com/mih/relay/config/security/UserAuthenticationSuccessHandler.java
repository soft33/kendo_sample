package com.mih.relay.config.security;

import java.io.IOException;


import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
// import org.springframework.security.web.DefaultRedirectStrategy;
// import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.authentication.SavedRequestAwareAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.stereotype.Component;

@Component
public class UserAuthenticationSuccessHandler extends SavedRequestAwareAuthenticationSuccessHandler {

	protected Logger logger = LoggerFactory.getLogger(this.getClass());
	private RequestCache requestCache = new HttpSessionRequestCache();
	// private RedirectStrategy redirectStratgy = new DefaultRedirectStrategy();
	
	// public UserAuthenticationSuccessHandler(String defaultTargetUrl) {
    //     setDefaultTargetUrl(defaultTargetUrl);
    // }

	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication auth)
			throws IOException, ServletException {
		//setDefaultTargetUrl("/main");
		logger.debug("#####UserAuthenticationSuccessHandler.onAuthenticationSuccess() ####");
		logger.debug("#####auth.getAuthorities() = "+auth.getAuthorities().toString());
		SavedRequest savedRequest = requestCache.getRequest(request, response);
		if(savedRequest != null){
			logger.debug("#####savedRequest.getRedirectUrl() = "+savedRequest.getRedirectUrl());
		}

		response.sendRedirect("/main");
		// super.onAuthenticationSuccess(request, response, auth);
		// SavedRequest savedRequest = requestCache.getRequest(request, response);
		// if(savedRequest != null) {
		// 	String targetUrl = savedRequest.getRedirectUrl();
		// 	redirectStratgy.sendRedirect(request, response, targetUrl);
		// } else {
		// 	super.onAuthenticationSuccess(request, response, auth);
		// }
	}
}
