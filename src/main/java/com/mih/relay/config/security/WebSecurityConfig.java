package com.mih.relay.config.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.servlet.PathRequest;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    // 정적 자원에 대해서는 Security 설정을 적용하지 않음.
    @Override
    public void configure(WebSecurity web) {
        web.ignoring().requestMatchers(PathRequest.toStaticResources().atCommonLocations());
        web.ignoring().antMatchers("/img/**");
    }
 
    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }

    @Autowired
    private UserAuthenticationProvider authProvider;

    @Autowired
    private UserAuthenticationSuccessHandler successHandler;

    @Autowired
    private UserAuthenticationFailureHandler failureHander;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests()
        //정적자원 
        .requestMatchers(PathRequest.toStaticResources().atCommonLocations()).permitAll()
        // 페이지 권한 설정
        // .antMatchers("/admin").hasRole("ADMIN")
        // .antMatchers("/user").hasRole("USER")
        .anyRequest().authenticated()
        .and()
        .csrf()
        // .csrfTokenRepository(CookieCsrfTokenRepository.withHttpOnlyFalse())
        .and() // 로그인 설정
            .formLogin()
            .loginPage("/login")
            .defaultSuccessUrl("/main")
            .successHandler(successHandler)
            .failureHandler(failureHander)
            .usernameParameter("memberId")
            .passwordParameter("memberPassword")
            .permitAll()
        .and() // 로그아웃 설정
            .logout()
            .logoutRequestMatcher(new AntPathRequestMatcher("/logout"))
            .logoutSuccessUrl("/login")
            .invalidateHttpSession(true)
        .and()// 403 예외처리 핸들링
            .exceptionHandling().accessDeniedPage("/common/error403");

        http.authenticationProvider(authProvider);
    }
}
