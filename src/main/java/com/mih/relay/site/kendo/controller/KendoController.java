package com.mih.relay.site.kendo.controller;

import com.mih.relay.common.base.BaseController;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * © 2020 MIH, Inc. All rights reserved. 
 * 
 * KENDO 샘플링 메뉴 Controller
 * 
 * KendoController.java 
 * version : 1.0.0 
 * creation : 2020. 12. 10. 
 * author : lcg
 */
@Controller
@RequestMapping("/kendo")
public class KendoController extends BaseController {
    
    /**
     * 기본 RequestMapping 함수 별도 로직 추가시 해당 함수 상단에 별도 정의하여 사용
     * 
     * @param prefix url prefix
     * @return String
     */
    @RequestMapping("/{prefix}")
    public String callPage(@PathVariable String prefix) {
        log.debug("#####prefix = " + prefix);
        return viewRootPath + prefix;
    }
}
