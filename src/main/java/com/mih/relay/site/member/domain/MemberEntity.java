package com.mih.relay.site.member.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data 
@AllArgsConstructor 
@NoArgsConstructor(access = AccessLevel.PUBLIC) 
@Entity(name="MEMBER")
public class MemberEntity {

    @Id 
    @GeneratedValue(strategy = GenerationType.IDENTITY) 
    @Column(name = "MEM_SEQ")
    private int memSeq;

    @Column(name = "MEM_ID")
    private String memId;

    @Column(name = "MEM_PASS")
    private String memPass;

    @Column(name = "MEM_ROLE")
    private String memRole;

}
