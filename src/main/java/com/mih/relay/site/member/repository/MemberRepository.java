package com.mih.relay.site.member.repository;

import com.mih.relay.site.member.domain.MemberEntity;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MemberRepository extends JpaRepository<MemberEntity,Long>{
    
    MemberEntity findByMemId(String memId);
}