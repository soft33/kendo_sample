package com.mih.relay.site.root.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import com.mih.relay.common.base.BaseController;


/**
 * © 2020 MIH, Inc. All rights reserved. 
 * 
 * RootPath처리를 위한 Controller
 * 
 * RootController.java 
 * version : 1.0.0 
 * creation : 2020. 12. 10. 
 * author : lcg
 */
@Controller
public class RootController extends BaseController {

    @RequestMapping("/")
    public String rootPage() {
        return "main";
    }

    @RequestMapping("/main")
    public String mainPage() {
        return "main";
    }

    @RequestMapping("/login")
    public String loginPage() {
        return "login";
    }

}
