package com.mih.relay.site.util.controller;

import com.mih.relay.common.base.BaseController;
import com.mih.relay.site.util.service.ApiService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import lombok.extern.slf4j.Slf4j;

/**
 * © 2020 MIH, Inc. All rights reserved. 
 * 
 * 유틸 샘플링 메뉴 Controller
 * 
 * UtilController.java 
 * version : 1.0.0 
 * creation : 2020. 12. 10. 
 * author : lcg
 */
@Slf4j
@Controller
@RequestMapping("/util")
public class UtilController extends BaseController {

    @Autowired
    ApiService apiService;

    /**
     * 맵페이지
     * 
     * @return String
     */
    @RequestMapping("/map")
    public String pageMap() {
        return viewRootPath + "map";
    }

    /**
     * 맵클러스터페이지
     * 
     * @return String
     */
    @RequestMapping("/mapCluster")
    public String pageMapCluster() {
        return viewRootPath + "mapCluster";
    }

    /**
     * 맵마커페이지
     * 
     * @return String
     */
    @RequestMapping("/mapMarker")
    public String pageMapMarker() {
        return viewRootPath + "mapMarker";
    }

    /**
     * api페이지
     * 
     * @return String
     */
    @RequestMapping("/api")
    public String pageApi() {
        return viewRootPath + "api";
    }
    
    /**
     *  사업자 등록 번호 조회
     * 
     * @param String 
     * @return String
     */
    @GetMapping("/companyNo")
    @ResponseBody
    public String getCompanyNo(@RequestParam(value="companyNo") String companyNo) {
        return apiService.getCompanyNo(companyNo);
    }

}
