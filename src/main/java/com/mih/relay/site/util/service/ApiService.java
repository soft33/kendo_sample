package com.mih.relay.site.util.service;

import org.springframework.stereotype.Service;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import lombok.extern.slf4j.Slf4j;

import java.io.IOException;
import java.io.StringReader;
import java.net.URISyntaxException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

@Slf4j
@Service
public class ApiService {

    /**
     *  국세청 사업자 등록 번호 조회
     * 
     * @param String 
     * @return String
     */
    public String getCompanyNo(String companyNo) {

        String retrunMsg = "";

        try {
            URIBuilder builder = new URIBuilder(
                    "https://teht.hometax.go.kr/wqAction.do?actionId=ATTABZAA001R08&screenId=UTEABAAA13&popupYn=false&realScreenId=");

            HttpPost httpPost = new HttpPost(builder.build());

            String reqXmlStr = "<map id='ATTABZAA001R08'><pubcUserNo/><mobYn>N</mobYn><inqrTrgtClCd>1</inqrTrgtClCd><txprDscmNo>"
                    + companyNo
                    + "</txprDscmNo><dongCode>05</dongCode><psbSearch>Y</psbSearch><map id='userReqInfoVO'/></map>";

            StringEntity se = new StringEntity(reqXmlStr, "UTF-8");
            se.setContentType("application/xml");
            se.setChunked(true);

            httpPost.setEntity(se);

            final CloseableHttpClient httpClient = HttpClientBuilder.create().build();
            final CloseableHttpResponse res = httpClient.execute(httpPost);

            if (res.getStatusLine().getStatusCode() == 200) {
                final ResponseHandler<String> handler = new BasicResponseHandler();
                String resXmlStr = handler.handleResponse(res);

                DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
                DocumentBuilder db = dbf.newDocumentBuilder();
                InputSource is = new InputSource();
                is.setCharacterStream(new StringReader(resXmlStr));
                Document doc = db.parse(is);
                retrunMsg = doc.getElementsByTagName("smpcBmanTrtCntn").item(0).getTextContent();

            }else{
                retrunMsg = "error"; 
            }
        } catch (IOException | URISyntaxException | ParserConfigurationException | SAXException e) {
            log.error(e.getMessage(), e);
            e.printStackTrace();
        }
        return retrunMsg;
    }
}
